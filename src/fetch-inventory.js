import ng from 'ng';

export default function(tag=NaN) {
  var promise = ng.$http.get(`http://lucee.kleinsteel.com/rest/Inventory/tag/${tag}`);

  return promise.then(response => response.data);
}
