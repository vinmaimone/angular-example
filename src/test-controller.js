import fetchTag from './fetch-inventory';

export default class TestCtrl {
  constructor() {
    this.searchInput = '';
    this.previousSearch = '';
    this.searchResult = {};
  }

  setSearchParam(val) {
    this.searchInput = val;
  }

  update() {
    var tagNum = this.searchInput;

    this.previousSearch = tagNum;

    if(tagNum) fetchTag(tagNum).then(items => this.searchResult = items);
  }
}

