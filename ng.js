import 'angular';

/* an attempt to cache the injector so this isnt so costly */
var angularDoc = angular.element(document),
    cache = {},
    injector = null;

function _injector() {
  if(injector) {
    return injector;
  } else {
    return (injector = angularDoc.injector())
  }
}

export default {

  get $injector() {
    console.log('getter for $injector called');
    return _injector();
  },

  get $http() {
    return cache['$http'] || ( cache['$http'] = _injector().get('$http') )
  },

  get $q() {
    return cache['$q'] || ( cache['$q'] = _injector().get('$q') )
  },

  get $location() {
    return cache['$location'] || ( cache['$location'] = _injector().get('$location') )
  },

  get $controller() {
    return cache['$controller'] || ( cache['$controller'] = _injector().get('$controller') )
  },

  get $compile() {
    return cache['$compile'] || ( cache['$compile'] = _injector().get('$compile') )
  },

  get $rootScope() {
    return cache['$rootScope'] || ( cache['$rootScope'] = _injector().get('$rootScope') )
  },

  get Scope() {
    return this.$rootScope.$new.bind(this.$rootScope)
  },

  bootstrap(app) {
    angularDoc.ready(function() {
      angular.bootstrap(document, [app.name]);
      document.documentElement.setAttribute('ng-app', app.name);
      console.log(`Application ${app.name} bootstrapped on document.documentElement`);
    })
  }

}



