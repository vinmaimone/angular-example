import ng from 'ng';
import TestCtrl from './src/test-controller';
import template from './src/template.html!text';

console.log('ng =', window.ng = ng);

var app = angular.module('DoATest',[]);

app.directive('testing', function() {
  return {
    restrict: 'AE',
    scope: { searchInput: '=' },
    bindToController: true,
    controller: TestCtrl,
    controllerAs: 'vm',
    template: template,
    link: function(scope,el,attrs,ctrl) {
      el.css({
        display: 'block',
        width: '320px',
        margin: '100px auto',
        padding: '5px'
      })
    }
  }
})





ng.bootstrap(app)
